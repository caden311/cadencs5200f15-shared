﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages.StreamMessages
{
    [DataContract]
    public class HaveAPenny : StreamMessage
    {
        [DataMember]
        public Penny Penny { get; set; }
    }
}
