﻿using System;
using System.Runtime.Serialization;
using SharedObjects;
using System.Net;

using Messages;

namespace Messages.RequestMessages
{
    [DataContract]
    public class Request : Message
    {
        [DataMember]
        public PublicEndPoint remoteEp { set; get; }
    }
}
