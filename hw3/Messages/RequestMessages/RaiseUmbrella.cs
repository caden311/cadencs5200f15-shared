﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages.RequestMessages
{
    [DataContract]
    public class RaiseUmbrella
    {
        [DataMember]
        public Umbrella Umbrella { get; set; }
    }
}
