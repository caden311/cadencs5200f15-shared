﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

using SharedObjects;
using log4net;
using Messages.AuctionMessages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using Messages.StatusMessages;

namespace Messages
{
    [DataContract]
    public class Message
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Message));

        private static List<Type> serializableTypes = new List<Type>()
        {
            typeof(AliveRequest),
            typeof(AuctionMessage),
            typeof(AuctionStarted),
            typeof(AuctionStatus),
            typeof(BalloonReply),
            typeof(BuyBalloonRequest),
            typeof(DeadProcessNotification),
            typeof(EndGame),
            typeof(FillBalloonRequest),
            typeof(GameListRequest),
            typeof(GameListReply),
            typeof(HitNotification),
            typeof(JoinGameReply),
            typeof(JoinGameRequest),
            typeof(LeaveGameRequest),
            typeof(LoginRequest),
            typeof(LoginReply),
            typeof(LogoutRequest),
            typeof(LowerUmbrella),
            typeof(NextIdRequest),
            typeof(NextIdReply),
            typeof(RaiseUmbrella),
            typeof(ReadyToStart),
            typeof(RegisterGameRequest),
            typeof(RegisterGameReply),
            typeof(Request),
            typeof(Reply),
            typeof(ShutdownRequest),
            typeof(StatusMessage),
            typeof(ThrowBalloonRequest),
            typeof(Umbrella),
            typeof(ValidateProcessRequest)
        };

        [DataMember]
        public MessageNumber MessageNr { get; set; }
        [DataMember]
        public MessageNumber ConversationId { get; set; }

        public Message()
        {
        }

        public void InitMessageAndConversationNumbers()
        {
            SetMessageAndConversationNumbers(MessageNumber.Create());
        }

        public void SetMessageAndConversationNumbers(MessageNumber msgNr)
        {
            SetMessageAndConversationNumbers(msgNr, msgNr.Clone());
        }

        public void SetMessageAndConversationNumbers(MessageNumber msgNr, MessageNumber convId)
        {
            MessageNr = msgNr;
            ConversationId = convId;
        }

        public byte[] Encode()
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Message), serializableTypes);
            MemoryStream mstream = new MemoryStream();
            serializer.WriteObject(mstream, this);

            return mstream.ToArray();
        }

        public static Message Decode(byte[] bytes)
        {
            Message result = null;
            if (bytes != null)
            {
                try
                {
                    MemoryStream mstream = new MemoryStream(bytes);
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Message), serializableTypes);
                    result = (Message)serializer.ReadObject(mstream);
                }
                catch (Exception err)
                {
                    _logger.WarnFormat("Except warning in decoding a message: {0}", err.Message);
                }
            }
            return result;
        }

    }
}
