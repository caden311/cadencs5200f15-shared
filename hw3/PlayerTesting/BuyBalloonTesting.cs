﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using Player;
namespace PlayerTesting
{
    [TestClass]
    public class BuyBalloonTesting
    {
        [TestMethod]
        public void BuyBalloon()
        {
            MessageNumber.LocalProcessId = 10;


            CommSubsystem system1 = CommSubTesting.TestUtilities.SetupTestCommSubsystem();
            CommProcessState state1 = new CommProcessState();
            state1.myPennies.Add(new Penny());
            state1.myPennies.Add(new Penny());
            BuyBallonProtocol buyBallon = new BuyBallonProtocol() { CommSubsystem = system1 };
            buyBallon.ProcessState = state1;
            buyBallon.ProcessState.RegistryInfo = new ProcessInfo();
            buyBallon.ProcessState.MyProcessInfo = new ProcessInfo();

            
            PublicEndPoint testEndPoint = new PublicEndPoint() { IPEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1200) };
            GameInfo[] games = new GameInfo[1];
            games[0] = new GameInfo() { GameId = 3, GameManager = new ProcessInfo() { EndPoint = testEndPoint } };

            Dictionary<int,ProcessInfo> processes = new Dictionary<int,ProcessInfo>();
            processes.Add(1,new ProcessInfo() { Type = ProcessInfo.ProcessType.BalloonStore, EndPoint = testEndPoint, gameId = 1 });
            processes.Add(2,new ProcessInfo() { Type = ProcessInfo.ProcessType.WaterServer, EndPoint = testEndPoint, gameId = 1 });
            buyBallon.ProcessState.gameProcesses = processes;
           
            buyBallon.ProcessState.RegistryInfo.EndPoint = testEndPoint;
            Assert.AreSame(system1, buyBallon.CommSubsystem);

            Request request1 = buyBallon.createRequest();

            Assert.IsTrue(request1 is BuyBalloonRequest);
  


            MessageNumber testNumber = new MessageNumber() { ProcessId = 1, SeqNumber = 1 };
            BalloonReply reply1 = new BalloonReply() { ConversationId = testNumber, MessageNr = testNumber, Success = true };

            buyBallon.ProcessState.MyProcessInfo.NumberOfUnfilledBalloon = 0;

            Assert.AreEqual(buyBallon.ProcessState.MyProcessInfo.NumberOfUnfilledBalloon, 0);

            buyBallon.update(reply1);
            Assert.AreEqual(buyBallon.ProcessState.MyProcessInfo.NumberOfUnfilledBalloon, 1);
            
        }
    }
}
