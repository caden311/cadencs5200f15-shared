﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using Player;

namespace PlayerTesting
{
    [TestClass]
    public class FillBalloonTesting
    {
        [TestMethod]
        public void FillBalloon()
        {

            CommSubsystem system1 = CommSubTesting.TestUtilities.SetupTestCommSubsystem();
            CommProcessState state1 = new CommProcessState();
            FillBalloonProtocol fillBalloon = new FillBalloonProtocol() { CommSubsystem = system1 };
            state1.myUnfilledBalloons.Add(new Balloon() { IsFilled = false });
            state1.myUnfilledBalloons.Add(new Balloon() { IsFilled = false });
            state1.myPennies.Add(new Penny());
            state1.myPennies.Add(new Penny());
            fillBalloon.ProcessState = state1;
            fillBalloon.ProcessState.RegistryInfo = new ProcessInfo();
            fillBalloon.ProcessState.MyProcessInfo = new ProcessInfo();


            PublicEndPoint testEndPoint = new PublicEndPoint() { IPEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1200) };

            Dictionary<int, ProcessInfo> processes = new Dictionary<int, ProcessInfo>();
            processes.Add(1, new ProcessInfo() { Type = ProcessInfo.ProcessType.BalloonStore, EndPoint = testEndPoint, gameId = 1 });
            processes.Add(2, new ProcessInfo() { Type = ProcessInfo.ProcessType.WaterServer, EndPoint = testEndPoint, gameId = 1 });
            fillBalloon.ProcessState.gameProcesses = processes;
            fillBalloon.ProcessState.RegistryInfo.EndPoint = testEndPoint;
            Assert.AreSame(system1, fillBalloon.CommSubsystem);

            Request request1 = fillBalloon.createRequest();

            Assert.IsTrue(request1 is FillBalloonRequest);



            MessageNumber testNumber = new MessageNumber() { ProcessId = 1, SeqNumber = 1 };
            BalloonReply reply1 = new BalloonReply() { ConversationId = testNumber, MessageNr = testNumber, Success = true };

            fillBalloon.ProcessState.MyProcessInfo.NumberOfUnfilledBalloon = 1;
            fillBalloon.ProcessState.MyProcessInfo.NumberOfFilledBalloon = 0;

            Assert.AreEqual(fillBalloon.ProcessState.MyProcessInfo.NumberOfUnfilledBalloon, 1);

            fillBalloon.update(reply1);
            Assert.AreEqual(fillBalloon.ProcessState.MyProcessInfo.NumberOfUnfilledBalloon, 0);
            Assert.AreEqual(fillBalloon.ProcessState.MyProcessInfo.NumberOfFilledBalloon, 1);
        }
    }
}
