﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Player;
namespace PlayerTesting
{
    [TestClass]
    public class ShutDownTesting
    {
        [TestMethod]
        public void ShutDownTest()
        {
            CommSubsystem system1 = CommSubTesting.TestUtilities.SetupTestCommSubsystem();
            CommProcessState state1 = new CommProcessState();
            state1.MyProcessInfo = new ProcessInfo() { Status = ProcessInfo.StatusCode.Registered };
            system1.Initialize();
            system1.Start();
            MessageNumber testNumber = new MessageNumber() { ProcessId = 1, SeqNumber = 1 };
            PublicEndPoint testEndPoint = new PublicEndPoint() { IPEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1250) };
            ShutdownRequest reqeust1 = new ShutdownRequest();

            Envelope testEnv = new Envelope() { EP = testEndPoint, Message = reqeust1 };


            ShutDownProtocol alive1 = new ShutDownProtocol() { CommSubsystem = system1, ProcessState = state1, IncomingEnvelope = testEnv };

            alive1.Execute(reqeust1);

            Assert.AreEqual(ProcessInfo.StatusCode.Terminating, state1.MyProcessInfo.Status);

        }
    }
}
