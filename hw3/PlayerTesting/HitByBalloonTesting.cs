﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using Player;

namespace PlayerTesting
{
    [TestClass]
    public class HitByBalloonTesting
    {
        [TestMethod]
        public void HitByBalloon()
        {
            CommSubsystem system1 = CommSubTesting.TestUtilities.SetupTestCommSubsystem();
            CommProcessState state1 = new CommProcessState();
            HitByBalloonProtocol hitByBalloon = new HitByBalloonProtocol() { CommSubsystem = system1 };
            hitByBalloon.ProcessState = state1;
            hitByBalloon.ProcessState.RegistryInfo = new ProcessInfo();
            hitByBalloon.ProcessState.MyProcessInfo = new ProcessInfo();


            PublicEndPoint testEndPoint = new PublicEndPoint() { IPEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1200) };


            hitByBalloon.ProcessState.RegistryInfo.EndPoint = testEndPoint;
            Assert.AreSame(system1, hitByBalloon.CommSubsystem);

           


            MessageNumber testNumber = new MessageNumber() { ProcessId = 1, SeqNumber = 1 };
            HitNotification reply1 = new HitNotification() { ByPlayerId = 2 };

            hitByBalloon.ProcessState.MyProcessInfo.LifePoints = 2;
            
            Assert.AreEqual(hitByBalloon.ProcessState.MyProcessInfo.LifePoints, 2);

            hitByBalloon.Execute(reply1);
            Assert.AreEqual(hitByBalloon.ProcessState.MyProcessInfo.LifePoints, 1);
            
        }
    }
}
