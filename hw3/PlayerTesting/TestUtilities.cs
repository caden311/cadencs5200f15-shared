﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CommSub;

namespace CommSubTesting
{
    static class TestUtilities
    {
        public static CommSubsystem SetupTestCommSubsystem()
        {
            CommSubsystem commSubsystem = new CommSubsystem()
            {
                MinPort = 12000,
                MaxPort = 12099,
                ConversationFactory = new DummyConversationFactory()
            };
            commSubsystem.Initialize();
            commSubsystem.Start();

            return commSubsystem;
        }
    }
}
