﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using Player;

namespace PlayerTesting
{
    [TestClass]
    public class ThrowBalloonTesting
    {
        [TestMethod]
        public void ThrowBalloon()
        {
            CommSubsystem system1 = CommSubTesting.TestUtilities.SetupTestCommSubsystem();
            CommProcessState state1 = new CommProcessState();
            ThrowBalloonProtocol throwBalloon = new ThrowBalloonProtocol() { CommSubsystem = system1 };
            state1.myFilledBalloons.Add(new Balloon() { IsFilled = true });
            state1.gameProcesses.Add(1, new ProcessInfo() { Type = ProcessInfo.ProcessType.Player, ProcessId=1, LifePoints=50 });
            throwBalloon.ProcessState = state1;
            throwBalloon.ProcessState.RegistryInfo = new ProcessInfo();
            throwBalloon.ProcessState.MyProcessInfo = new ProcessInfo();


            PublicEndPoint testEndPoint = new PublicEndPoint() { IPEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1200) };


            throwBalloon.ProcessState.RegistryInfo.EndPoint = testEndPoint;
            Assert.AreSame(system1, throwBalloon.CommSubsystem);

            GameInfo[] games = new GameInfo[1];
            games[0] = new GameInfo() { GameId = 3 };
            throwBalloon.ProcessState.avaliableGames = games;
            throwBalloon.ProcessState.RegistryInfo.EndPoint = testEndPoint;
            Request request1 = throwBalloon.createRequest();

            Assert.IsTrue(request1 is ThrowBalloonRequest);
  

            MessageNumber testNumber = new MessageNumber() { ProcessId = 1, SeqNumber = 1 };
            Reply reply1 = new Reply() { Success=true};

            throwBalloon.ProcessState.MyProcessInfo.NumberOfFilledBalloon = 2;

            Assert.AreEqual(throwBalloon.ProcessState.MyProcessInfo.NumberOfFilledBalloon, 2);

            throwBalloon.update(reply1);
            Assert.AreEqual(throwBalloon.ProcessState.MyProcessInfo.NumberOfFilledBalloon, 1);
        }
    }
}
