﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Player;

namespace PlayerTesting
{
    [TestClass]
    public class JoinGameProtocolTesting
    {
        [TestMethod]
        public void JoinGame()
        {
            MessageNumber.LocalProcessId = 10;


            CommSubsystem system1 = CommSubTesting.TestUtilities.SetupTestCommSubsystem();
            CommProcessState state1 = new CommProcessState();
            JoinGameProtocol JoinGame1 = new JoinGameProtocol() { CommSubsystem = system1 };
            JoinGame1.ProcessState = state1;
            JoinGame1.ProcessState.RegistryInfo = new ProcessInfo();
            JoinGame1.ProcessState.MyProcessInfo = new ProcessInfo();
             PublicEndPoint testEndPoint = new PublicEndPoint() { IPEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1200) };
            GameInfo[] games = new GameInfo[1];
            games[0] = new GameInfo() { GameId = 3, GameManager = new ProcessInfo() { EndPoint = testEndPoint } };

            JoinGame1.ProcessState.avaliableGames = games;
           
            JoinGame1.ProcessState.RegistryInfo.EndPoint = testEndPoint;
            Assert.AreSame(system1, JoinGame1.CommSubsystem);

            Request request1 = JoinGame1.createRequest();

            Assert.IsTrue(request1 is JoinGameRequest);
            Assert.AreEqual(request1.remoteEp, testEndPoint);


            MessageNumber testNumber = new MessageNumber() { ProcessId = 1, SeqNumber = 1 };
            JoinGameReply reply1 = new JoinGameReply() { ConversationId = testNumber, MessageNr = testNumber, Success = true};
            
            JoinGame1.update(reply1);

            Assert.AreEqual(JoinGame1.ProcessState.MyProcessInfo.Status, ProcessInfo.StatusCode.JoinedGame);
            

        }
    }
}
