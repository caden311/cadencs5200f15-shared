﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Player;
namespace PlayerTesting
{
    [TestClass]
    public class GameListTesting
    {
        [TestMethod]
        public void GameList()
        {

            MessageNumber.LocalProcessId = 10;


            CommSubsystem system1 = CommSubTesting.TestUtilities.SetupTestCommSubsystem();
            CommProcessState state1 = new CommProcessState();
            GameListProtocol GameList1 = new GameListProtocol() { CommSubsystem = system1 };
            GameList1.ProcessState = state1;
            GameList1.ProcessState.RegistryInfo = new ProcessInfo();
            GameList1.ProcessState.MyProcessInfo = new ProcessInfo();
            PublicEndPoint testEndPoint = new PublicEndPoint() { IPEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1200) };
            GameList1.ProcessState.RegistryInfo.EndPoint = testEndPoint;
            Assert.AreSame(system1, GameList1.CommSubsystem);

            Request request1 = GameList1.createRequest();

            Assert.IsTrue(request1 is GameListRequest);
            Assert.AreEqual(request1.ConversationId.ProcessId, 10);
            Assert.AreEqual(request1.ConversationId.SeqNumber, 1);
            Assert.AreEqual(request1.remoteEp, testEndPoint);
            


            MessageNumber testNumber = new MessageNumber() { ProcessId = 1, SeqNumber = 1 };
            GameInfo[] games = new GameInfo[1];
            games[0] = new GameInfo() { GameId = 3 };
           
            Reply reply1 = new GameListReply() { GameInfo=games, ConversationId = testNumber, MessageNr = testNumber, Success = true };
            GameList1.update(reply1);

            Assert.AreEqual(GameList1.ProcessState.avaliableGames.Length,1);
            Assert.AreEqual(GameList1.ProcessState.avaliableGames[0].GameId, 3);

        }
    }
}
