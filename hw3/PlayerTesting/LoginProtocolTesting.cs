﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Player;

namespace PlayerTesting
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void LoginTesting()
        {

            MessageNumber.LocalProcessId = 10;


            CommSubsystem system1 = CommSubTesting.TestUtilities.SetupTestCommSubsystem();
            CommProcessState state1 = new CommProcessState();
            LoginProtocol login1 = new LoginProtocol() { CommSubsystem = system1 };
            login1.ProcessState = state1;
            login1.ProcessState.RegistryInfo = new ProcessInfo();
            PublicEndPoint testEndPoint = new PublicEndPoint() { IPEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1200) };
            login1.ProcessState.RegistryInfo.EndPoint= testEndPoint;
            Assert.AreSame(system1, login1.CommSubsystem);
            
            Request request1=login1.createRequest();

            Assert.IsTrue(request1 is LoginRequest);
            Assert.AreEqual(request1.ConversationId.ProcessId, 10);
            Assert.AreEqual(request1.ConversationId.SeqNumber, 1);
            Assert.AreEqual(request1.remoteEp, testEndPoint);
            
            
            MessageNumber testNumber=new MessageNumber(){ ProcessId=1, SeqNumber=1};
            LoginReply reply1 = new LoginReply() { ConversationId = testNumber, MessageNr = testNumber, Success = true, ProcessInfo = new ProcessInfo() { ProcessId = 5 } };
            login1.update(reply1);

            Assert.AreEqual(login1.ProcessState.MyProcessInfo.Status, ProcessInfo.StatusCode.Registered);
            Assert.AreEqual(login1.ProcessState.MyProcessInfo.ProcessId, 5);


        }
    }
}
