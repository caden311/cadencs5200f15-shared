﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Player;
namespace PlayerTesting
{
    [TestClass]
    public class AliveRequestProtocolTesting
    {
        [TestMethod]
        public void AliveRequest()
        {
            CommSubsystem system1 = CommSubTesting.TestUtilities.SetupTestCommSubsystem();
            CommProcessState state1 = new CommProcessState();
            system1.Initialize();
            system1.Start();
            MessageNumber testNumber=new MessageNumber(){ ProcessId=1, SeqNumber=1};
            PublicEndPoint testEndPoint = new PublicEndPoint() { IPEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1250) };
            AliveRequest reqeust1=new AliveRequest(){ remoteEp=testEndPoint, ConversationId=testNumber, MessageNr=testNumber};

            Envelope testEnv=new Envelope(){ EP=testEndPoint, Message=reqeust1};


            AliveRequestProtocol alive1 = new AliveRequestProtocol() { CommSubsystem = system1, ProcessState = state1, IncomingEnvelope = testEnv };

            alive1.Execute(reqeust1);

      //      Assert.AreEqual(system1.Communicator.IncomingAvailable(), 1);
        }
    }
}
