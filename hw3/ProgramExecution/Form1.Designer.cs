﻿namespace ProgramExecution
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LifePoints = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Pennys = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.UnfilledBallons = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.FilledBalloons = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.GameId = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ProcessId = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Status = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LifePoints
            // 
            this.LifePoints.AutoSize = true;
            this.LifePoints.Location = new System.Drawing.Point(108, 36);
            this.LifePoints.Name = "LifePoints";
            this.LifePoints.Size = new System.Drawing.Size(13, 13);
            this.LifePoints.TabIndex = 0;
            this.LifePoints.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "LifePoints:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Pennys:";
            // 
            // Pennys
            // 
            this.Pennys.AutoSize = true;
            this.Pennys.Location = new System.Drawing.Point(108, 67);
            this.Pennys.Name = "Pennys";
            this.Pennys.Size = new System.Drawing.Size(13, 13);
            this.Pennys.TabIndex = 3;
            this.Pennys.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "EmptyBalloons:";
            // 
            // UnfilledBallons
            // 
            this.UnfilledBallons.AutoSize = true;
            this.UnfilledBallons.Location = new System.Drawing.Point(108, 98);
            this.UnfilledBallons.Name = "UnfilledBallons";
            this.UnfilledBallons.Size = new System.Drawing.Size(13, 13);
            this.UnfilledBallons.TabIndex = 5;
            this.UnfilledBallons.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "FilledBalloons";
            // 
            // FilledBalloons
            // 
            this.FilledBalloons.AutoSize = true;
            this.FilledBalloons.Location = new System.Drawing.Point(111, 132);
            this.FilledBalloons.Name = "FilledBalloons";
            this.FilledBalloons.Size = new System.Drawing.Size(13, 13);
            this.FilledBalloons.TabIndex = 7;
            this.FilledBalloons.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(201, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "GameId:";
            // 
            // GameId
            // 
            this.GameId.AutoSize = true;
            this.GameId.Location = new System.Drawing.Point(252, 13);
            this.GameId.Name = "GameId";
            this.GameId.Size = new System.Drawing.Size(10, 13);
            this.GameId.TabIndex = 9;
            this.GameId.Text = " ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(204, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "ProcessId:";
            // 
            // ProcessId
            // 
            this.ProcessId.AutoSize = true;
            this.ProcessId.Location = new System.Drawing.Point(267, 50);
            this.ProcessId.Name = "ProcessId";
            this.ProcessId.Size = new System.Drawing.Size(0, 13);
            this.ProcessId.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 294);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Status:";
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Location = new System.Drawing.Point(68, 294);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(0, 13);
            this.Status.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 372);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ProcessId);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.GameId);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.FilledBalloons);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.UnfilledBallons);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Pennys);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LifePoints);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LifePoints;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Pennys;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label UnfilledBallons;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label FilledBalloons;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label GameId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label ProcessId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Status;
    }
}

