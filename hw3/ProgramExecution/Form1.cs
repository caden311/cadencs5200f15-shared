﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;

using Player;
using CommSub;
using BalloonStore;
using SharedObjects;

namespace ProgramExecution
{
    public partial class Form1 : Form
    {
        MyPlayer player = new MyPlayer();

        public Form1()
        {

            InitializeComponent();

            Thread playerThread = new Thread(player.PlayGame);

            playerThread.Start();

            //BalloonStorePlayer player = new BalloonStorePlayer();

            //Thread playerThread = new Thread(player.PlayGame);

            //playerThread.Start();

            

            player.PlayerStatus += ChangeStatusLabel;

            player.shutDown += shutHerDown;
    


        }

        public void shutHerDown()
        {
            player.CommSubsystem.Stop();
            this.Invoke((MethodInvoker)delegate
            {
               
                this.Close();
            });

        }
        
        public void ChangeStatusLabel(CommProcessState state)
        {
            if (InvokeRequired)
            {

                MyPlayer.DisplayStatus disp = new MyPlayer.DisplayStatus(ChangeStatusLabel);
                Invoke(disp, state);
            }
            else
            {
                GameId.Text = state.MyProcessInfo.gameId.ToString();
                ProcessId.Text = MessageNumber.LocalProcessId.ToString();
                Status.Text = state.MyProcessInfo.StatusString;
                Pennys.Text = state.MyProcessInfo.NumberOfPennies.ToString();
                FilledBalloons.Text = state.MyProcessInfo.NumberOfFilledBalloon.ToString();
                UnfilledBallons.Text = state.MyProcessInfo.NumberOfUnfilledBalloon.ToString();
                    LifePoints.Text = state.MyProcessInfo.LifePoints.ToString();
               
            }
    

        }
        
    }
}
