﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Messages.ReplyMessages;

namespace Player
{
    public class LeaveGameProtocol:RequestReplyConv
    {
        public override Messages.RequestMessages.Request createRequest()
        {
            LeaveGameRequest leaveGame = new LeaveGameRequest();
       
            leaveGame.remoteEp = this.ProcessState.JoinedGameEndPoint;
            leaveGame.InitMessageAndConversationNumbers();
       
            
            return leaveGame;
        }

        public override void update(Messages.Message msg)
        {
            Reply reply = msg as Reply;
            if (reply.Success)
            {
                this.ProcessState.MyProcessInfo.Status = ProcessInfo.StatusCode.Registered;
            }
            
        }
    }
}
