﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Messages.ReplyMessages;

namespace Player
{
    public class FillBalloonProtocol:RequestReplyConv
    {
        public override Request createRequest()
        {
            FillBalloonRequest fillBalloon = new FillBalloonRequest();
            
   
                fillBalloon.InitMessageAndConversationNumbers();
                fillBalloon.Balloon = this.ProcessState.myUnfilledBalloons[0];
                foreach (var process in this.ProcessState.gameProcesses.Values)
                {
                    if (process.Type == ProcessInfo.ProcessType.WaterServer)
                    {
                        fillBalloon.remoteEp = process.EndPoint;
                    }
                }
                
                fillBalloon.Pennies = new Penny[] { this.ProcessState.myPennies[0], this.ProcessState.myPennies[1]};
                
            

            return fillBalloon;

        }

        public override void update(Messages.Message msg)
        {
            BalloonReply reply = msg as BalloonReply;
            if (reply.Success)
            {
                this.ProcessState.MyProcessInfo.NumberOfPennies -= 2;
                this.ProcessState.myPennies.RemoveAt(1);
                this.ProcessState.myPennies.RemoveAt(0);
                
                //Need to add ballon to my balloons
                this.ProcessState.myFilledBalloons.Add(reply.Balloon);
                this.ProcessState.MyProcessInfo.NumberOfFilledBalloon += 1;

                this.ProcessState.myUnfilledBalloons.RemoveAt(0);
                this.ProcessState.MyProcessInfo.NumberOfUnfilledBalloon -= 1;
            }
        }
    }
}
