﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;

namespace Player
{
    public class JoinGameProtocol:RequestReplyConv
    {
        public override Messages.RequestMessages.Request createRequest()
        {
            JoinGameRequest JR = new JoinGameRequest();
            JR.InitMessageAndConversationNumbers();
            JR.Player = this.ProcessState.MyProcessInfo;

            
            JR.GameId = this.ProcessState.avaliableGames[0].GameId;
            JR.remoteEp = this.ProcessState.avaliableGames[0].GameManager.EndPoint;
            
            return JR;


        }

        public override void update(Messages.Message msg)
        {
            JoinGameReply JR = msg as JoinGameReply;
            if (JR.Success == true)
            {
                this.ProcessState.MyProcessInfo.LifePoints = (Int16)JR.InitialLifePoints;
                this.ProcessState.MyProcessInfo.gameId = this.ProcessState.avaliableGames[0].GameId;
                this.ProcessState.JoinedGameEndPoint = this.ProcessState.avaliableGames[0].GameManager.EndPoint;
                this.ProcessState.MyProcessInfo.Status = ProcessInfo.StatusCode.JoinedGame;
            }


        }
    }
}
