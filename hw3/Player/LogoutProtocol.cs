﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;

namespace Player
{
    public class LogoutProtocol:RequestReplyConv
    {
        public override Messages.RequestMessages.Request createRequest()
        {
            LogoutRequest LR = new LogoutRequest();
            LR.remoteEp = this.ProcessState.RegistryInfo.EndPoint;
            return LR;
        }

        public override void update(Messages.Message msg)
        {
            this.ProcessState.MyProcessInfo.Status = ProcessInfo.StatusCode.Terminating;
        }

    }
}
