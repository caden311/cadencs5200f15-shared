﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;


namespace Player
{
    public class AliveRequestProtocol:Conversation
    {


        public override void Execute(object context)
        {

            PublicEndPoint registryAddress = this.IncomingEnvelope.EP;

            MessageNumber newNumber = MessageNumber.Create();

            Reply aliveReply = new Reply()
            {
  
                Note = "Reply",
                Success = true
            };

            aliveReply.SetMessageAndConversationNumbers(MessageNumber.Create(), IncomingEnvelope.Message.ConversationId);

            this.CommSubsystem.Communicator.Send(new Envelope() { Message = aliveReply, EP = registryAddress });


        }
    }
}
