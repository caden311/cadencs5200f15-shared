﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;

namespace Player
{
    public class HitByBalloonProtocol:Conversation
    {

        public override void Execute(object context)
        {
            this.ProcessState.MyProcessInfo.LifePoints -= 1;


            PublicEndPoint gameAddress = new PublicEndPoint();

            gameAddress = this.ProcessState.JoinedGameEndPoint;
           
            MessageNumber newNumber = MessageNumber.Create();
            Reply hitReply = new Reply()
            {

                Note = "Reply",
                Success = true
            };

            hitReply.InitMessageAndConversationNumbers();

            this.CommSubsystem.Communicator.Send(new Envelope() { Message = hitReply, EP = gameAddress });
        }
    }
}
