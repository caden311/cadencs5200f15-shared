﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Messages.StreamMessages;
using Messages.StatusMessages;
using Messages.ReplyMessages;

namespace Player
{
    public class PlayerConvFactory:ConversationFactory
    {
        public override void Initialize()
        {

            Add(typeof(ThrowBalloonRequest), typeof(ThrowBalloonProtocol));
            Add(typeof(FillBalloonRequest), typeof(FillBalloonProtocol));
            Add(typeof(BuyBalloonRequest), typeof(BuyBallonProtocol));
            Add(typeof(HitNotification), typeof(HitByBalloonProtocol));
            Add(typeof(ReadyToStart), typeof(ReadyToStartProtocol));
            Add(typeof(LogoutRequest), typeof(LogoutProtocol));
            Add(typeof(JoinGameRequest), typeof(JoinGameProtocol));
            Add(typeof(AliveRequest), typeof(AliveRequestProtocol));
            Add(typeof(LoginRequest), typeof(LoginProtocol));
            Add(typeof(GameListRequest),typeof(GameListProtocol));
            Add(typeof(ShutdownRequest), typeof(ShutDownProtocol));
            Add(typeof(LeaveGameRequest), typeof(LeaveGameProtocol));
            Add(typeof(EndGame), typeof(EndGameProtocol));

        }
    }
}
