﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;


using CommSub;
using SharedObjects;
using Messages;
using Utils;

namespace Player
{
    public class MyPlayer:CommProcess
    {
        public delegate void ShutDown ();
        public event ShutDown shutDown;

        public delegate void DisplayStatus(CommProcessState status);
        public event DisplayStatus PlayerStatus;

        public PlayerState gameState { set; get; }
    
        
       
        public MyPlayer()
        {
            PublicEndPoint registryAddress = new PublicEndPoint();
            registryAddress.IPEndPoint = new IPEndPoint(IPAddress.Loopback, 12000);
            //registryAddress.IPEndPoint = new IPEndPoint(IPAddress.Parse("52.3.213.61"), 12000);
            //add avaliable games to player process state.  Specilize a process state.
            CommProcessState PlayerState = new PlayerState() { MyProcessInfo = new ProcessInfo(), RegistryInfo = new ProcessInfo() };
            PlayerState.gameProcesses = new Dictionary<int,ProcessInfo>();
            PlayerState.RegistryInfo.EndPoint = registryAddress;
            
           
            ConversationFactory CF = new PlayerConvFactory() { };
            CF.ProcessState = PlayerState;

         //  system.ConversationFactory = CF;
          // process = new PlayerCommProcess(PlayerState);

           this.Options = new PlayerRunTimeOptions();
           this.SetupCommSubsystem(CF);
           this.Start();
           this.myState = PlayerState;
           myState.RaiseStateChangedEvent();     
      
        }

        public void PlayGame()
        {
            //system.Initialize();
            //system.Start();

            PlayerStatus(this.myState);
            RequestReplyConv login = new LoginProtocol();
            login.ProcessState = this.myState;
            login.Execute(this);
            this.myState.MyProcessInfo.LifePoints = 1;
            while(this.myState.MyProcessInfo.Status!=ProcessInfo.StatusCode.Terminating)
            {

                if (this.myState.MyProcessInfo.LifePoints <= 0)
                {
                    LeaveGameProtocol leave = new LeaveGameProtocol();
                    leave.Execute(this);
                }

                if (this.myState.MyProcessInfo.Status != ProcessInfo.StatusCode.JoiningGame &&
                   this.myState.MyProcessInfo.Status != ProcessInfo.StatusCode.JoinedGame &&
                   this.myState.MyProcessInfo.Status != ProcessInfo.StatusCode.PlayingGame)
                {
                    RequestReplyConv gameList = new GameListProtocol();
                    gameList.Execute(this);


                    if (this.myState.avaliableGames!=null&&this.myState.avaliableGames.Length > 0 && this.myState.MyProcessInfo.Status == ProcessInfo.StatusCode.Registered)
                    {

                        JoinGameProtocol joinGame = new JoinGameProtocol();
                        joinGame.ProcessState = this.myState;
                        joinGame.Execute(this);

                    }
                }
                else
                {

                    if (this.myState.MyProcessInfo.Status == ProcessInfo.StatusCode.JoinedGame)
                    {
                        PlayerStatus(this.myState);
                        if (this.myState.MyProcessInfo.LifePoints == 0)
                        {
                            LeaveGameProtocol leave = new LeaveGameProtocol();
                            leave.Execute(this);
                        }

                        if (this.myState.MyProcessInfo.NumberOfPennies > 0 && this.myState.MyProcessInfo.NumberOfUnfilledBalloon < 1)
                        {

                            BuyBallonProtocol buyBalloon = new BuyBallonProtocol();
                            buyBalloon.Execute(this);
                        }
                        else if (this.myState.MyProcessInfo.NumberOfUnfilledBalloon > 0 && this.myState.MyProcessInfo.NumberOfPennies >= 2)
                        {
                            FillBalloonProtocol fillBalloon = new FillBalloonProtocol();
                            fillBalloon.Execute(this);
                        }

                        if (this.myState.MyProcessInfo.NumberOfFilledBalloon > 0)
                        {
                            ThrowBalloonProtocol throwBalloon = new ThrowBalloonProtocol();
                            throwBalloon.Execute(this);
                        }
                    }
                }
            }
            shutDown();
        }

        protected override void Process(object state)
        {
          // throw new NotImplementedException();
        }
    }


}
