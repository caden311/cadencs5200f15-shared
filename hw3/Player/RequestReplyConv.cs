﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;

namespace Player
{
    public abstract class RequestReplyConv:Conversation
    {
        
        public abstract Request createRequest();
        public abstract void update(Message msg);



        public override void Execute(object context)
        {

            
            CommProcess process = context as CommProcess;
            CommSubsystem system = process.CommSubsystem;
            
           
            this.CommSubsystem = process.CommSubsystem;
            this.ProcessState = process.MyState;

            EnvelopeQueue q = system.QueueDictionary.RequestQueue;
          

            bool success = false;
            int tries = 0;
            Request myRequest = createRequest();
            

            
            Envelope env1 = new Envelope() { Message = myRequest, EP = myRequest.remoteEp };
            q.QueueId = myRequest.ConversationId;
            q=system.QueueDictionary.CreateQueue(q.QueueId);
            

            while (!success && tries < 3)
            {
                system.Communicator.Send(env1);
                Envelope returnEnv = q.Dequeue(3000);
                if (returnEnv!=null)
                {
                    success = true;
                    update(returnEnv.Message);
                }
                else
                    tries++;
               

            }
           
            system.QueueDictionary.CloseQueue(q.QueueId);
    
            

        }

       // protected abstract bool IsProcessValid();
      //  protected abstract bool IsConversationValid();
    }
}
