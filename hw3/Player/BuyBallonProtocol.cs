﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Messages.ReplyMessages;

namespace Player
{
    public class BuyBallonProtocol:RequestReplyConv
    {

        public override Request createRequest()
        {
            BuyBalloonRequest buyB = new BuyBalloonRequest();


            buyB.Penny = this.ProcessState.myPennies[0];
            
                foreach (var process in this.ProcessState.gameProcesses.Values)
                {
                        if (process.Type == ProcessInfo.ProcessType.BalloonStore)
                        {
                            buyB.remoteEp = process.EndPoint;
                        }
                }
                
                buyB.InitMessageAndConversationNumbers();
               

            return buyB;
            
        }

        public override void update(Message msg)
        {
            BalloonReply reply = msg as BalloonReply;
            if (reply.Success)
            {
                this.ProcessState.myPennies.RemoveAt(0);
                this.ProcessState.MyProcessInfo.NumberOfPennies -= 1;

                this.ProcessState.myUnfilledBalloons.Add(reply.Balloon);
                this.ProcessState.MyProcessInfo.NumberOfUnfilledBalloon += 1;
            }
        }
    }
}
