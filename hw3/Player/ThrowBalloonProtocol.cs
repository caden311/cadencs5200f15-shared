﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Messages.ReplyMessages;


namespace Player
{
    public class ThrowBalloonProtocol:RequestReplyConv
    {
        public override Request createRequest()
        {
            ThrowBalloonRequest throwRequest = new ThrowBalloonRequest();
            

                throwRequest.InitMessageAndConversationNumbers();
                throwRequest.remoteEp = this.ProcessState.JoinedGameEndPoint;
                int maxLife = 0;
                int throwId = -1;
                foreach (var process in this.ProcessState.gameProcesses.Values)
                {
                    if (process.Type == ProcessInfo.ProcessType.Player && process.ProcessId!=MessageNumber.LocalProcessId)
                    {
                        if (process.LifePoints > maxLife)
                        {
                            maxLife = process.LifePoints;
                            throwId = process.ProcessId;
                           
                        }
                       
                    }
                }
                if (throwId != -1)
                {
                    throwRequest.TargetPlayerId = throwId;
                }

                throwRequest.Balloon = this.ProcessState.myFilledBalloons[0];
                
            return throwRequest;
        }

        public override void update(Message msg)
        {
            Reply reply = msg as Reply;
            if (reply.Success)
            {
                this.ProcessState.MyProcessInfo.NumberOfFilledBalloon -= 1;
                this.ProcessState.myFilledBalloons.RemoveAt(0);
            }
        }
    }
}
