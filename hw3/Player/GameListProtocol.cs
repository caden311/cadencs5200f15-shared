﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;

namespace Player
{
    public class GameListProtocol:RequestReplyConv
    {


        public override Messages.RequestMessages.Request createRequest()
        {
         
            //
            GameListRequest GLR = new GameListRequest() { StatusFilter = (int)GameInfo.StatusCode.Available };
            GLR.InitMessageAndConversationNumbers();
            GLR.remoteEp = this.ProcessState.RegistryInfo.EndPoint;
            return GLR;
        }

        public override void update(Messages.Message msg)
        {

            GameListReply reply = msg as GameListReply;
            GameInfo[] info = reply.GameInfo;
            this.ProcessState.avaliableGames = info;

          //  if (this.ProcessState.MyProcessInfo.Status == ProcessInfo.StatusCode.JoinedGame)
           // {
            

            //}

            
        }
    }
}
