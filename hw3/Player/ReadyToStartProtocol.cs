﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.StatusMessages;
using Messages.StreamMessages;

namespace Player
{
    public class ReadyToStartProtocol:Conversation
    {
        public override void Execute(object context)
        {
            Envelope env = this.IncomingEnvelope;
            ReadyToStart readyToStartMessage = env.Message as ReadyToStart;

            ProcessInfo p1 = new ProcessInfo() { ProcessId = MessageNumber.LocalProcessId, Label = "Caden Joining Game", EndPoint = this.ProcessState.MyProcessInfo.EndPoint };
   //         InGame inGame = new InGame() { Process = p1, SeqNr = 1 };

                PublicEndPoint streamEndPoint = readyToStartMessage.StreamEP;
                TcpClient client = new TcpClient();
                client.Connect(streamEndPoint.Host, streamEndPoint.Port);

                NetworkStream stream = client.GetStream();
           

                while (client.Connected)
                {
                    if (this.ProcessState.MyProcessInfo.Status == ProcessInfo.StatusCode.JoiningGame ||
                        this.ProcessState.MyProcessInfo.Status == ProcessInfo.StatusCode.JoinedGame ||
                        this.ProcessState.MyProcessInfo.Status == ProcessInfo.StatusCode.PlayingGame)
                    {

                        stream.ReadTimeout = 1000;
                        StreamMessage msg = stream.ReadStreamMessage();

                        if (msg is InGame)
                        {
                            InGame msg1 = msg as InGame;
                            if(this.ProcessState.gameProcesses.ContainsKey(msg1.Process.ProcessId))
                            {
                                this.ProcessState.gameProcesses[msg1.Process.ProcessId] = msg1.Process;
                            }
                            else
                                this.ProcessState.gameProcesses.Add(msg1.Process.ProcessId,msg1.Process);
                        }
                        else if (msg is HaveAPenny)
                        {
                            HaveAPenny msg1 = msg as HaveAPenny;
                            this.ProcessState.MyProcessInfo.NumberOfPennies += 1;
                            this.ProcessState.myPennies.Add(msg1.Penny);


                        }
                        else if (msg is NotInGame)
                        {
                            NotInGame msg1 = msg as NotInGame;
                            this.ProcessState.gameProcesses.Remove(msg1.ProcessId);
                        }
                    }

                  
                    
                  
                }

        }
    }
}
