﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;

namespace Player
{
    public class PlayerRunTimeOptions:RuntimeOptions
    {
        public override void SetDefaults()
        {
            this.Alias = "Cade";
            this.ANumber = "A01195160";
            this.FirstName = "Caden";
            this.LastName = "Sorenson";

        }
    }
}
