﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;

namespace Player
{
    public class LoginProtocol:RequestReplyConv
    {
      

    
        public override Request createRequest()
        {
            
            LoginRequest LR = new LoginRequest()
            {
             
                Identity = new IdentityInfo() { Alias = "Jomama", ANumber = "A01195160", FirstName = "Caden", LastName = "Sorenson" },
                ProcessLabel = "Player Login",
                ProcessType = ProcessInfo.ProcessType.Player
            };
            LR.InitMessageAndConversationNumbers();
            
            LR.remoteEp = this.ProcessState.RegistryInfo.EndPoint;
            return LR;

        }
        public override void update(Message msg)
        {
            LoginReply LR=msg as LoginReply;
            MessageNumber.LocalProcessId = LR.ProcessInfo.ProcessId;
            this.ProcessState.MyProcessInfo = LR.ProcessInfo;
            this.ProcessState.MyProcessInfo.Status = ProcessInfo.StatusCode.Registered;
           
            //this.ProcessState.MyProcessInfo = LR.ProcessInfo;
           // this.ProcessState.MyProcessInfo.Status = ProcessInfo.StatusCode.Registered;
        }

    }
}
