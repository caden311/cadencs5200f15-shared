﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using CommSubTesting;
using BalloonStore;


namespace BalloonStoreTesting
{
    [TestClass]
    public class BalloonLeaveGameTesting
    {
        [TestMethod]
        public void BalloonLeaveGameTest()
        {
            MessageNumber.LocalProcessId = 10;


            CommSubsystem system1 = CommSubTesting.TestUtilities.SetupTestCommSubsystem(new BalloonStoreConvFactory());
            CommProcessState state1 = new CommProcessState();
            BalloonLeaveGameProtocol leave1 = new BalloonLeaveGameProtocol() { CommSubsystem = system1 };
            leave1.ProcessState = state1;
            leave1.ProcessState.RegistryInfo = new ProcessInfo();
            PublicEndPoint testEndPoint = new PublicEndPoint() { IPEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1200) };
            leave1.ProcessState.RegistryInfo.EndPoint = testEndPoint;
            Assert.AreSame(system1, leave1.CommSubsystem);




            MessageNumber testNumber = new MessageNumber() { ProcessId = 1, SeqNumber = 1 };
            LoginReply reply1 = new LoginReply() { ConversationId = testNumber, MessageNr = testNumber, Success = true, ProcessInfo = new ProcessInfo() { ProcessId = 5 } };
    
        }
    }
}
