﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using CommSubTesting;
using BalloonStore;

namespace BalloonStoreTesting
{
    [TestClass]
    public class BalloonJoinGameTesting
    {
        [TestMethod]
        public void BalloonJoinGameTest()
        {

            MessageNumber.LocalProcessId = 10;
            
            CommSubsystem system1 = CommSubTesting.TestUtilities.SetupTestCommSubsystem(new BalloonStoreConvFactory());
            
            CommProcessState state1 = new CommProcessState();
            state1.MyProcessInfo = new ProcessInfo();

            BalloonJoinGameProtocol joinGame = new BalloonJoinGameProtocol() { CommSubsystem = system1 };
            joinGame.ProcessState = state1;
            joinGame.ProcessState.RegistryInfo = new ProcessInfo();
            PublicEndPoint testEndPoint = new PublicEndPoint() { IPEndPoint = new IPEndPoint(IPAddress.Parse("1.1.1.1"), 12000) };
            joinGame.ProcessState.RegistryInfo.EndPoint = testEndPoint;
            Assert.AreSame(system1, joinGame.CommSubsystem);

            Request request1 = joinGame.createRequest();

            Assert.IsTrue(request1 is JoinGameRequest);
            
            Assert.AreEqual(request1.ConversationId.SeqNumber, 1);
            Assert.AreEqual(request1.remoteEp, testEndPoint);


            MessageNumber testNumber = new MessageNumber() { ProcessId = 1, SeqNumber = 1 };
            JoinGameReply reply1 = new JoinGameReply() { ConversationId = testNumber, MessageNr = testNumber, Success = true, GameId=1 };
            joinGame.update(reply1);

            Assert.AreEqual(joinGame.ProcessState.MyProcessInfo.NumberOfUnfilledBalloon, 100);
            

        }
    }
}
