﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using CommSubTesting;
using BalloonStore;

namespace BalloonStoreTesting
{
    [TestClass]
    public class BalloonLogoutTesting
    {
        [TestMethod]
        public void BalloonLogoutTest()
        {


            CommSubsystem system1 = CommSubTesting.TestUtilities.SetupTestCommSubsystem(new BalloonStoreConvFactory());
            CommProcessState state1 = new CommProcessState();
            BalloonLogoutProtocol Logout1 = new BalloonLogoutProtocol() { CommSubsystem = system1 };
            Logout1.ProcessState = state1;
            Logout1.ProcessState.RegistryInfo = new ProcessInfo();
            PublicEndPoint testEndPoint = new PublicEndPoint() { IPEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1200) };
            Logout1.ProcessState.RegistryInfo.EndPoint = testEndPoint;
            Assert.AreSame(system1, Logout1.CommSubsystem);

            Request request1 = Logout1.createRequest();

            Assert.IsTrue(request1 is LogoutRequest);
            Assert.AreEqual(request1.remoteEp, testEndPoint);
            
        }
    }
}
