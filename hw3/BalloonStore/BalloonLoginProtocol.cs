﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;


namespace BalloonStore
{
    public class BalloonLoginProtocol:BalloonRequestReplyConv
    {
        public override Request createRequest()
        {
            LoginRequest LR = new LoginRequest()
            {

                Identity = new IdentityInfo() { Alias = "BStore", ANumber = "A01195160", FirstName = "Caden's balloon store", LastName = "" },
                ProcessLabel = "Balloon Store Login",
                ProcessType = ProcessInfo.ProcessType.BalloonStore
            };
            LR.InitMessageAndConversationNumbers();

            LR.remoteEp = this.ProcessState.RegistryInfo.EndPoint;
            return LR;

        }

        public override void update(Message msg)
        {
            LoginReply LR = msg as LoginReply;
            MessageNumber.LocalProcessId = LR.ProcessInfo.ProcessId;
            this.ProcessState.MyProcessInfo = LR.ProcessInfo;
            this.ProcessState.MyProcessInfo.Status = ProcessInfo.StatusCode.Registered;
         
        }
    }
}
