﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Security.Cryptography;


using CommSub;
using SharedObjects;
using Messages;
using Utils;


namespace BalloonStore
{
    public class BalloonStorePlayer:CommProcess
    {
           public BalloonStorePlayer()
           {
            PublicEndPoint registryAddress = new PublicEndPoint();
            registryAddress.IPEndPoint = new IPEndPoint(IPAddress.Loopback, 12000);

            //add avaliable games to player process state.  Specilize a process state.
            CommProcessState balloonState = new BalloonState() { MyProcessInfo = new ProcessInfo(), RegistryInfo = new ProcessInfo() };
            balloonState.gameProcesses = new Dictionary<int,ProcessInfo>();
            balloonState.RegistryInfo.EndPoint = registryAddress;
            
           
            ConversationFactory CF = new BalloonStoreConvFactory() { };
            CF.ProcessState = balloonState;

         //  system.ConversationFactory = CF;
          // process = new PlayerCommProcess(PlayerState);

           this.Options = new BalloonRunTimeOptions();
           this.SetupCommSubsystem(CF);
           this.Start();
           this.myState = balloonState;
           myState.RaiseStateChangedEvent();     
      
        }

        public void PlayGame()
        {
            BalloonRequestReplyConv login = new BalloonLoginProtocol();
            login.ProcessState = this.myState;
            login.Execute(this);

            
                 // JoinGameProtocol joinGame = new JoinGameProtocol();
             //  joinGame.ProcessState = this.myState;
           // joinGame.Execute(this);
            while (this.myState.MyProcessInfo.Status != ProcessInfo.StatusCode.Terminating)
            {


                if (this.myState.MyProcessInfo.Status != ProcessInfo.StatusCode.JoiningGame &&
                   this.myState.MyProcessInfo.Status != ProcessInfo.StatusCode.JoinedGame &&
                   this.myState.MyProcessInfo.Status != ProcessInfo.StatusCode.PlayingGame)
                {

                    
                    RSACryptoServiceProvider rsa = this.myState.GetProcessRSA(this.myState.MyProcessInfo.ProcessId);
                    this.myState.AddProcessRSA(this.myState.MyProcessInfo.ProcessId, rsa);
                    RSAParameters keyInfo = rsa.ExportParameters(true);

                    for (int i = 0; i < 100; i++)
                    {
                        byte[] byteArr=new byte[keyInfo.Exponent.Length+keyInfo.Modulus.Length];
                        System.Buffer.BlockCopy(keyInfo.Exponent, 0, byteArr, 0, keyInfo.Exponent.Length);
                        System.Buffer.BlockCopy(keyInfo.Modulus, 0, byteArr, keyInfo.Exponent.Length, keyInfo.Modulus.Length);
                        this.myState.myUnfilledBalloons.Add(new Balloon() { Id = i, IsFilled = false, DigitalSignature=byteArr });

                    }
                       


                }


            }

        }
        protected override void Process(object state)
        {
            

        }
    }
}
