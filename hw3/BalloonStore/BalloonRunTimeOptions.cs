﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;


using CommSub;
using SharedObjects;
using Messages;
using Utils;

namespace BalloonStore
{
    public class BalloonRunTimeOptions:RuntimeOptions
    {
        public override void SetDefaults()
        {
            this.Alias = "BStore";
            this.ANumber = "A01195160";
            this.FirstName = "Caden's Balloon Store";
            this.LastName = "last name store";
           
        }
    }
}
