﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;


using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;
using Messages.StreamMessages;
using Messages.StatusMessages;
using Messages.ReplyMessages;

namespace BalloonStore
{
    public class BalloonStoreConvFactory : ConversationFactory
    {

        public override void Initialize()
        {
            Add(typeof(LoginRequest), typeof(BalloonLoginProtocol));
            Add(typeof(ShutdownRequest), typeof(BalloonShutdownProtocol));
            Add(typeof(LeaveGameRequest), typeof(BalloonLeaveGameProtocol));
            Add(typeof(LogoutRequest), typeof(BalloonLogoutProtocol));
            Add(typeof(JoinGameRequest), typeof(BalloonJoinGameProtocol));

        }
    }
}
