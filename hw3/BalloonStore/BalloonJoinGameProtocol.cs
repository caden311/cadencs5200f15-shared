﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Security.Cryptography;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;


namespace BalloonStore
{
    public class BalloonJoinGameProtocol:BalloonRequestReplyConv
    {
 

        public override Request createRequest()
        {
            //get cmd line args // test values for now

            
            PublicEndPoint gmep = new PublicEndPoint() { IPEndPoint = new IPEndPoint(IPAddress.Parse("1.1.1.1"), 12000) };
            int gameid = 1;
            

            
            Request JoinGame = new JoinGameRequest() { GameId = gameid, remoteEp = gmep, Player = this.ProcessState.MyProcessInfo };
            JoinGame.InitMessageAndConversationNumbers();

            return JoinGame;

        }

        public override void update(Message msg)
        {
            Reply reply = msg as Reply;

            if (reply.Success)
            {
                this.ProcessState.MyProcessInfo.NumberOfUnfilledBalloon = 100;
                


            }
            
        }
    }
}
