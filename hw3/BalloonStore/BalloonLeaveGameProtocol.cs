﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using CommSub;
using SharedObjects;
using Messages;
using Utils;
using Messages.RequestMessages;


namespace BalloonStore
{
    public class BalloonLeaveGameProtocol:BalloonRequestReplyConv
    {
        public override Messages.RequestMessages.Request createRequest()
        {
            Request leaveGame = new LeaveGameRequest();
            PublicEndPoint gameAddress = new PublicEndPoint();

            for (int i = 0; i < this.ProcessState.avaliableGames.Length; i++)
            {
                if (this.ProcessState.avaliableGames[i].GameId == this.ProcessState.MyProcessInfo.gameId)
                    gameAddress = this.ProcessState.avaliableGames[i].GameManager.EndPoint;


            }

            leaveGame.remoteEp = gameAddress;
            leaveGame.InitMessageAndConversationNumbers();

            return leaveGame;
        }

        public override void update(Messages.Message msg)
        {
            Reply reply = msg as Reply;
            if (reply.Success)
            {
                this.ProcessState.MyProcessInfo.Status = ProcessInfo.StatusCode.Registered;
            }

        }
    }
}
