﻿using System;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommSub;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using SharedObjects;

namespace CommSubTesting
{
    [TestClass]
    public class DoerTesting
    {
        [TestMethod]
        public void Doer()
        {
            // Setup a fake process id
            MessageNumber.LocalProcessId = 10;

            // Create a commSubsystem with a Listener
            CommSubsystem commSubsystem = TestUtilities.SetupTestCommSubsystem(new DummyConversationFactory());

            PublicEndPoint targetEP = new PublicEndPoint()
            {
                Host = "127.0.0.1",
                Port = commSubsystem.Communicator.Port
            };
            commSubsystem.Initialize();
            commSubsystem.Start();


            Communicator remoteComm = new Communicator() { MinPort = 12000, MaxPort = 12099 };
            remoteComm.Start();

            MessageNumber nr1 = MessageNumber.Create();
            AliveRequest request = new AliveRequest() { MessageNr = nr1, ConversationId = nr1 };
            Envelope env1 = new Envelope() { Message = request, EP = targetEP };
            remoteComm.Send(env1);

            Thread.Sleep(2000);
            Assert.IsTrue(DummyConversation.LastCreatedInstance.ExecuteWasCalled);
            Assert.AreEqual(0, commSubsystem.QueueDictionary.RequestQueue.Count);


        }
    }
}
