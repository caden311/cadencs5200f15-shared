﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommSub;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using SharedObjects;
namespace CommSubTesting
{
    [TestClass]
    public class CommProcessStateTesting
    {
        [TestMethod]
        public void StateTesting()
        {
            ProcessInfo info1 = new ProcessInfo() { };
            ProcessInfo info2 = new ProcessInfo() { };


            CommProcessState state1 = new CommProcessState() { MyProcessInfo = info1, RegistryInfo = info2, Quit = false };
            Assert.AreEqual(state1.Quit, false);
            Assert.AreSame(state1.MyProcessInfo, info1);
            Assert.AreSame(state1.RegistryInfo, info2);
            Assert.IsNotNull(state1.StatusString);

        }
    }
}
