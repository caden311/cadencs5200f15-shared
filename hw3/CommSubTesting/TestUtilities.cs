﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CommSub;

namespace CommSubTesting
{
    public static class TestUtilities
    {
        public static CommSubsystem SetupTestCommSubsystem(ConversationFactory factory)
        {
            CommSubsystem commSubsystem = new CommSubsystem()
            {
                MinPort = 12000,
                MaxPort = 12099,
                ConversationFactory = factory
            };
            commSubsystem.Initialize();
            commSubsystem.Start();

            return commSubsystem;
        }
    }
}
