﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

using SharedObjects;
using CommSub;
using Utils;

using log4net;


namespace CommSub
{
    public class CommProcessState : State
    {
        #region Private Data Members
        private static readonly ILog log = LogManager.GetLogger(typeof(CommProcessState));

        private CommSubsystem commSubsystem = new CommSubsystem();

        protected RSACryptoServiceProvider myRSA = new System.Security.Cryptography.RSACryptoServiceProvider();
        protected RSAPKCS1SignatureFormatter myDigitalSignatureCreator = null;
        protected ConcurrentDictionary<Int32, RSACryptoServiceProvider> processRSAs = new ConcurrentDictionary<int, RSACryptoServiceProvider>();

        #endregion

        #region Public Process Stuff
        public ProcessInfo MyProcessInfo { get; set; }
        public ProcessInfo RegistryInfo { get; set; }
        public GameInfo[] avaliableGames { set; get; }
       // public List<ProcessInfo> gameProcesses { set; get; }
        public Dictionary<int, ProcessInfo> gameProcesses = new Dictionary<int, ProcessInfo>();
        public PublicEndPoint JoinedGameEndPoint { set; get; }
        public List<Penny> myPennies = new List<Penny>();
        public List<Balloon> myUnfilledBalloons = new List<Balloon>();
        public List<Balloon> myFilledBalloons = new List<Balloon>();

        public override string StatusString
        {
            get
            {
                return (MyProcessInfo == null) ? base.StatusString : MyProcessInfo.StatusString;
            }
        }

        public void Dispose()
        {
            lock (myLock)
            {
                if (commSubsystem != null)
                    commSubsystem.Stop();
                commSubsystem = null;
            }
        }

        public void AddProcessRSA(int processId, RSACryptoServiceProvider rsa)
        {
            if (processRSAs.ContainsKey(processId))
                processRSAs[processId] = rsa;
            else
                processRSAs.TryAdd(processId, rsa);
        }

        public List<KeyValuePair<int, RSACryptoServiceProvider>> OtherProcessRSAs { get { return processRSAs.ToList(); } }

        public RSACryptoServiceProvider GetProcessRSA(int processId)
        {
            RSACryptoServiceProvider rsa = null;
            processRSAs.TryGetValue(processId, out rsa);
            return rsa;
        }

        public RSACryptoServiceProvider MyRSA { get { return myRSA; } }
        public RSAPKCS1SignatureFormatter MyDigitalSignatureCreator
        {
            get
            {
                SetupDigitalSignatureCreator();
                return myDigitalSignatureCreator;
            }
        }

        public void ChangeStatus(ProcessInfo.StatusCode newStatus)
        {
            MyProcessInfo.Status = newStatus;
            RaiseStateChangedEvent();
        }

        public void DoShutdown()
        {
            MyProcessInfo.Status = ProcessInfo.StatusCode.Terminating;
            Quit = true;
            RaiseShutdownEvent();
        }

        public new void Reset()
        {
            log.Debug("Entering Reset");
            processRSAs = new ConcurrentDictionary<int, RSACryptoServiceProvider>();
            base.Reset();
            log.Debug("Leaving Reset");
        }

        #endregion

        protected void SetupDigitalSignatureCreator()
        {
            if (myDigitalSignatureCreator == null)
            {
                myDigitalSignatureCreator = new RSAPKCS1SignatureFormatter(MyRSA);
                myDigitalSignatureCreator.SetHashAlgorithm("SHA1");
            }
        }
    }
}
