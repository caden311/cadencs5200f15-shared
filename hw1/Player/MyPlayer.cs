﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

using Messages;
using SharedObjects;

namespace Player
{
    class MyPlayer
    {
        public IdentityInfo playerInfo { set; get; }
        public UdpClient client { set; get; }
        public GameList myGameList { set; get; }
        public IPEndPoint remotEp { set; get; }
        public IPEndPoint EP { set; get; }
        public JoinGame myJoinGame { set; get; }
        public MyPlayer(UdpClient c, IPEndPoint re, IPEndPoint ep, GameList myList, JoinGame myGame)
        {
            client = c;
            myGameList = myList;
            remotEp = re;
            EP = ep;
            myJoinGame = myGame;

        }
    }
}
