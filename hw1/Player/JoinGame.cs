﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

using Messages;
using SharedObjects;

namespace Player
{
    class JoinGame:MyPlayer
    {
        public JoinGame(UdpClient c, IPEndPoint re, IPEndPoint ep)
            : base(c, re, ep, null,null)
        {}

        public void JoinGameRequest(ProcessInfo joinInfo, int gameID )
        {

            JoinGameRequest JR = new JoinGameRequest();
            JR.Player = joinInfo;
            JR.Player.Type = ProcessInfo.ProcessType.Player;
            JR.GameId = gameID;
            Console.WriteLine("attempting to joing game: {0}", gameID);
            Console.WriteLine("info: {0}", joinInfo.ToString());
            

          //  LR.ProcessLabel = "Login Request";
            byte[] bytes = JR.Encode();

          IPEndPoint gameEndPoint=new IPEndPoint(IPAddress.Parse(joinInfo.EndPoint.Host),joinInfo.EndPoint.Port);

            int result = client.Send(bytes, bytes.Length, gameEndPoint);
           // Console.WriteLine("Send Result: {0}", result);


        }


        public void JoinGameReply(Message data)
        {


            JoinGameReply reply = data as JoinGameReply;
      
                    Console.WriteLine("Joing game is: {0}",reply.Success);
                      Console.WriteLine("game id: {0}",reply.GameId);
        
        }

    }
}
