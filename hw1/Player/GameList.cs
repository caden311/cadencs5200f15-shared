﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

using Messages;
using SharedObjects;

namespace Player
{
    class GameList:MyPlayer
    {
        public GameList(UdpClient c, IPEndPoint re, IPEndPoint ep, JoinGame myGame)
            : base(c, re, ep, null,myGame)
        {
        }
        //public UdpClient client { set; get; }

        public void GameRequest()
        {
            GameListRequest GLR = new GameListRequest();
            GLR.StatusFilter = (int)GameInfo.StatusCode.Available;
            //GLR.StatusFilter = -1;

            byte[] bytes = GLR.Encode();
            

            int result = client.Send(bytes, bytes.Length, this.remotEp);
            Console.WriteLine("Send Result: {0}", result);

        }


        public void GameReply(Message data)
        {
            

                   
                     GameListReply gameReply = data as GameListReply;
                     
                  //  GLR = data as GameList;
                    
                     GameInfo[] info = gameReply.GameInfo;
                    
                     for (int i = 0; i < info.Length; i++)
                     {
                         Console.WriteLine("Attemping to join gameID: {0}",info[i].GameId);

                        myJoinGame.JoinGameRequest(info[i].GameManager, info[i].GameId);
                         break;

                     }
                  
                     Console.WriteLine("Game List reply: {0}",gameReply.Success);
                

        }

    }
}
