﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

using Messages;
using SharedObjects;

namespace Player
{
    class Logout : MyPlayer
    {

        // public UdpClient client;
        public Logout(UdpClient c, IPEndPoint re, IPEndPoint ep)
            : base(c, re, ep, null,null)
        { }

        public void LogoutRequest()
        {
            LogoutRequest LR = new LogoutRequest();
          
            byte[] bytes = LR.Encode();


            //IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse("52.3.213.61"),12000);
            //IPEndPoint remoteEP = new IPEndPoint(IPAddress.Loopback, 12000);

            int result = client.Send(bytes, bytes.Length, this.remotEp);
            Console.WriteLine("Logout Send Result: {0}", result);

        }





    }
}
