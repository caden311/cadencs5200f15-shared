﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;

using Messages;
using SharedObjects;

namespace Player
{
    class Listener:MyPlayer
    {
        public bool killThread;
        //public UdpClient client { set; get; }
        //public GameList myGameList { set; get; }

        public Listener(UdpClient c, IPEndPoint re, IPEndPoint ep, GameList MyGameList, JoinGame myJoinGame)
            : base(c, re, ep, MyGameList, myJoinGame)
        { }


        public void Listen()
        {
   
            bool done = false;

            try
            {
                while (!done)
                {
                   
                    Console.WriteLine("Waiting for requests...");
                    IPEndPoint myEP = this.EP;
                    byte[] bytes = client.Receive(ref myEP);

                    Message data = Message.Decode(bytes);

                    if (data is AliveRequest)
                    {
                        PlayerAliveReply aliveRequest = new PlayerAliveReply(this.client,this.remotEp,this.EP);
                        aliveRequest.client = client;
                      aliveRequest.AliveReply();

                    }
                    else if (data is GameListReply)
                    {
                       // GameList myGameList = new GameList();
                      //  myGameList.client = client;
                        myGameList.GameReply(data);
                    }
                    else if (data is JoinGameReply)
                    {
                        myJoinGame.JoinGameReply(data);
                    }


              
                    Thread.Sleep(1000);
                    done = killThread;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());

            }

        }


    }
}
