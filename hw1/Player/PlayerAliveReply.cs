﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

using Messages;
using SharedObjects;

namespace Player
{
   
    
    class PlayerAliveReply:MyPlayer
    {
       
        public PlayerAliveReply(UdpClient c, IPEndPoint re, IPEndPoint ep)
            : base(c, re, ep, null,null)
        { }

        //public UdpClient client { set; get; }

        public void AliveReply()
        {
           
   
                    Reply alive = new Reply();
                    alive.Note = "Reply";
                    alive.Success = true;
                       // Message alive= new Message();
                        
                        byte[] b = alive.Encode();

                        int result = client.Send(b, b.Length, this.remotEp);
                        Console.WriteLine("Send Result of alive reply: {0}", result);

       

        }
    }
}
