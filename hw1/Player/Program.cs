﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;

using Messages;
using SharedObjects;

namespace Player
{
    class Program
    {
        static void Main(string[] args)
        {
            //IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse("52.3.213.61"),12000);
            IPEndPoint remoteEP = new IPEndPoint(IPAddress.Loopback, 12000);
            IPEndPoint localEP = new IPEndPoint(IPAddress.Any, 0);
            UdpClient client = new UdpClient(localEP);
            JoinGame myJoinGame = new JoinGame(client,remoteEP,localEP);
            GameList myGameList = new GameList(client,remoteEP,localEP,myJoinGame);
            MyPlayer me = new MyPlayer(client,remoteEP,localEP,myGameList,myJoinGame);
            me.playerInfo = new IdentityInfo() { Alias = "Buddy", ANumber = "A01195160", FirstName = "Caden", LastName = "Sorenson" };
            Login L = new Login(me.client,me.remotEp,me.EP);
            //L.client = me.client;
           L.LoginRequest();
           L.LoginReply();

           
           myGameList.client = me.client;
           myGameList.GameRequest();
           

   

          Listener myListener = new Listener(me.client,me.remotEp,me.EP,me.myGameList,myJoinGame);
         myListener.client=me.client;
         myListener.myGameList = me.myGameList;
   
         Thread ListenerThread = new Thread(myListener.Listen);
         ListenerThread.Start();
           
         

         
            Console.Read();

            Logout logout = new Logout(me.client,me.remotEp,me.EP);
            logout.LogoutRequest();

            Console.Read();

            myListener.killThread = true;
            ListenerThread.Join();
           
        }
    }
}
