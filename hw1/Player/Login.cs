﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

using Messages;
using SharedObjects;

namespace Player
{
    class Login:MyPlayer
    {

       // public UdpClient client;
        public Login(UdpClient c, IPEndPoint re, IPEndPoint ep)
            : base(c, re, ep, null,null)
        {}

        public void LoginRequest()
        {
            
            LoginRequest LR = new LoginRequest();
            LR.Identity = this.playerInfo;
            LR.ProcessType = ProcessInfo.ProcessType.Player;
           

            LR.ProcessLabel = "Login Request";
            byte[] bytes = LR.Encode();

          
            //IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse("52.3.213.61"),12000);
            //IPEndPoint remoteEP = new IPEndPoint(IPAddress.Loopback, 12000);

            int result = client.Send(bytes, bytes.Length, this.remotEp);
            Console.WriteLine("Send Result: {0}", result);

        }


        public void LoginReply()
        {
            
            //IPEndPoint EP = new IPEndPoint(IPAddress.Parse("52.3.213.61"), 12000);
            //IPEndPoint EP = new IPEndPoint(IPAddress.Any, 0);
            LoginReply LR = new LoginReply();

            bool done=false;

            try 
            {
                while (!done)
                {
                    Console.WriteLine("Waiting on Login...");
                    IPEndPoint myEp = this.EP;
                    byte[] bytes = client.Receive(ref myEp);

                    Message data = Message.Decode(bytes);

                    LR = data as LoginReply;

                    done = LR.Success;
                    Console.WriteLine(LR.Note);
                      
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            
            }
        
        }


    }
}
